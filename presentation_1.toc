\beamer@endinputifotherversion {3.32pt}
\select@language {portuguese}
\beamer@sectionintoc {1}{Introdu\IeC {\c c}\IeC {\~a}o}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Motiva\IeC {\c c}\IeC {\~a}o}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Problemas}{4}{0}{1}
\beamer@subsectionintoc {1}{3}{Objetivos}{5}{0}{1}
\beamer@subsectionintoc {1}{4}{Contribui\IeC {\c c}\IeC {\~o}es}{6}{0}{1}
\beamer@subsectionintoc {1}{5}{Metodologia}{7}{0}{1}
\beamer@sectionintoc {2}{Biblioteca Digital}{8}{0}{2}
\beamer@subsectionintoc {2}{1}{Bibliotecas Digitais}{8}{0}{2}
\beamer@subsectionintoc {2}{2}{Artefato Digital}{10}{0}{2}
\beamer@subsectionintoc {2}{3}{Padr\IeC {\~o}es de metadados}{11}{0}{2}
\beamer@sectionintoc {3}{Padr\IeC {\~a}o OAI-PMH}{12}{0}{3}
\beamer@subsectionintoc {3}{1}{Padr\IeC {\~a}o OAI-PMH}{12}{0}{3}
\beamer@sectionintoc {4}{Linked Data}{16}{0}{4}
\beamer@sectionintoc {5}{Participa.br}{18}{0}{5}
\beamer@subsectionintoc {5}{1}{O Participa.br}{19}{0}{5}
\beamer@subsectionintoc {5}{2}{Noosfero}{21}{0}{5}
\beamer@sectionintoc {6}{Desenvolvimento}{25}{0}{6}
\beamer@subsectionintoc {6}{1}{Ferramentas utilizadas}{26}{0}{6}
\beamer@subsectionintoc {6}{2}{Proposta de arquitetura}{27}{0}{6}
\beamer@sectionintoc {7}{Conclus\IeC {\~a}o}{29}{0}{7}
